package uk.cubed.pi.core;

import java.util.Map.Entry;
import java.util.UUID;
import java.util.logging.Logger;

import org.apache.logging.log4j.LogManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import uk.cubed.pi.command.CommandLoader;
import uk.cubed.pi.command.CommandManager;
import uk.cubed.pi.command.CommandSettings;
import uk.cubed.pi.gui.GUILoader;
import uk.cubed.pi.gui.GUIManager;

import uk.cubed.pi.settings.InitialisedPlayer;

public class PI {
	
	private Logger log = Bukkit.getLogger();
	private static PI INSTANCE;
	private CommandLoader commandLoader;
	private CommandManager commandManager;
	private CommandSettings commandSettings;
	private GUIManager guiManager;
	private GUILoader guiLoader;
	private static String prefix = "[" + ChatColor.GOLD + "PI3" + ChatColor.WHITE + "]:" + ChatColor.GOLD + " ";
	
	private String bracketColorANSI = "\u001B[32m";
	private String prefixColorANSI = "\u001B[35m";
	private String messageColorANSI = "\u001B[36m";
	
	private JavaPlugin jp;
	
	public void onEnable(JavaPlugin jp) {
		this.jp = jp;
		org.apache.logging.log4j.core.Logger logger = (org.apache.logging.log4j.core.Logger)LogManager.getRootLogger();
		logger.addFilter(new ConsoleFilter());
		INSTANCE = this;
		commandSettings = new CommandSettings();
		commandManager = new CommandManager();
		commandLoader = new CommandLoader("uk/cubed/pi/command/exe");
		guiManager = new GUIManager();
		guiLoader = new GUILoader("uk/cubed/pi/gui/exe");
	}
	
	public void onDisable() {
		if (getCommandSettings().getVerifiedPlayers().size() > 0) {
			for (Entry<UUID, InitialisedPlayer> entry: getCommandSettings().getVerifiedPlayers().entrySet()) {
				Player p = Bukkit.getPlayer(entry.getKey());
				//if (p != null) sendMessage(p, "Plugin disabled. Commands will not longer work.");
				if (p != null) getCommandManager().showInitialisedTitle(p, false);
			}
		}
	}
	
	public static PI getInstance() {
		return INSTANCE;
	}
	
	public CommandManager getCommandManager() {
		return commandManager;
	}
	
	public CommandLoader getCommandLoader() {
		return commandLoader;
	}
	
	public CommandSettings getCommandSettings() {
		return commandSettings;
	}
	
	public GUIManager getGUIManager() {
		return guiManager;
	}
	
	public GUILoader getGUILoader() {
		return guiLoader;
	}
	
	public String getName() {
		return jp.getName();
	}
	
	public JavaPlugin getPluginInstance() {
		return jp;
	}
	
	public void print(Object o) {
    	log.info(bracketColorANSI + "[" + prefixColorANSI + "PlayerSpy" + bracketColorANSI + "] " + messageColorANSI + String.valueOf(o) + "\u001B[0m");
    }

	public static void sendMessage(Player p, String s) {
		p.sendMessage(prefix + s);
	}
}
