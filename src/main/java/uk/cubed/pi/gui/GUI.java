package uk.cubed.pi.gui;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import uk.cubed.pi.core.PI;

public abstract class GUI implements Listener {
	
	private String guiName;
	private Inventory guiInv;
	
	public GUI(String guiName) {
		this.guiName = guiName;
	}

	public String getGUIName() {
		return this.guiName;
	}
	
	public Inventory getInv() {
		return this.guiInv;
	}
	
	public PI getInstance() {
		return PI.getInstance();
	}
	
	public void setGUIName(String setName) {
		guiName = setName;
	}
	
	public void setInv(Inventory setInv) {
		guiInv = setInv;
	}

	public abstract void showInventory(Player p);
	
	public void schedule(Runnable r) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(PI.getInstance().getPluginInstance(), r);
	}
	
	public void createItem(Material material, Inventory inv, int Slot, String name, String lore) {
		ItemStack item = new ItemStack(material);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		ArrayList<String> Lore = new ArrayList<String>();
		Lore.add(lore);
		meta.setLore(Lore);
		item.setItemMeta(meta);
		inv.setItem(Slot, item);
	}
	
	public Player getPlayer(String s, Player p) {
		PI pi = PI.getInstance();
		for (Player v: Bukkit.getOnlinePlayers()) {
			if (v.getName().equalsIgnoreCase(s)) {
				if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
					return v;
				} else {
					if (!pi.getCommandSettings().getVerifiedPlayers().containsKey(v)) {
						return v;
					} else {
						return p;
					}
				}
			}
		}
		return null;
	}
	
	public Player getAnyPlayer(String s) {
		for (Player v: Bukkit.getOnlinePlayers()) {
			if (v.getName().equalsIgnoreCase(s)) {
				return v;
			}
		}
		return null;
	}
}
