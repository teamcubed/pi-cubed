package uk.cubed.pi.gui;

import java.util.TreeMap;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import uk.cubed.pi.core.PI;

/**
 * @author Sparks
 *
 * Time: 18:03:17
 * Date: 9 Dec 2014
 */
public class GUIManager implements Listener {

	private TreeMap<String, GUI> guiMap = new TreeMap<>();
	
	public GUIManager() {
		Bukkit.getPluginManager().registerEvents(this, PI.getInstance().getPluginInstance());
	}
	
	public TreeMap<String, GUI> getGUIMap() {
		return guiMap;
	}
	
	public GUI getGUI(String guiName) {
		if(guiMap.containsKey(guiName.toLowerCase())) {
			return guiMap.get(guiName.toLowerCase());
		}
		return null;
	}
}
