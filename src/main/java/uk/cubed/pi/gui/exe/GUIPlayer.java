package uk.cubed.pi.gui.exe;

import java.util.LinkedList;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.command.Command.Category;
import uk.cubed.pi.core.PI;
import uk.cubed.pi.gui.GUI;

public class GUIPlayer extends GUI {
	
	public GUIPlayer() {
		super("player");
		this.setInv(Bukkit.getServer().createInventory(null, 54, "Player Commands"));
	}

	@Override
	public void showInventory(Player p) {
		LinkedList<Command> PlayerCMDs = new LinkedList<Command>();
		
		for (Entry<String, Command> entry: PI.getInstance().getCommandManager().getCommandMap().entrySet()) {
			if (entry.getValue().getCommandCategory() == Category.PLAYER || entry.getValue().getCommandCategory() == Category.ALL) {
				PlayerCMDs.add(entry.getValue());
			}
		}
		
		if (PlayerCMDs.size() == 0) {
			PI.sendMessage(p, "No commands to show.");
			return;
		}
		
		if (PlayerCMDs.size() <= 54) {
			for (int i = 0; i < PlayerCMDs.size(); i++) {
				Command cmd = PlayerCMDs.get(i);
				createItem(Material.REDSTONE, getInv(), i, cmd.getCommandName(), cmd.getCommandDescription());
			}
		} else {
			//TODO Make this shit here. Too layzee atm.
		}
		
		p.openInventory(getInv());
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void InventoryClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		if(e.getInventory().equals(getInv())){
			e.setCancelled(true);
			if(e.getCurrentItem() == null){
				return;
			} else if(e.getCurrentItem().getType() == Material.REDSTONE){
				p.closeInventory();
				GUI guiCommand = getInstance().getGUIManager().getGUI(e.getCurrentItem().getItemMeta().getDisplayName());
				if (guiCommand == null) {
					PI.sendMessage(p, "There is no GUI for this command. Try *" + e.getCurrentItem().getItemMeta().getDisplayName() + ".");
				} else {
					guiCommand.showInventory(p);
				}
			}
		}
	}
}
