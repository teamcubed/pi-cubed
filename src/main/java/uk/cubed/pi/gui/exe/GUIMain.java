package uk.cubed.pi.gui.exe;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;

import uk.cubed.pi.gui.GUI;

public class GUIMain extends GUI {
	
	public GUIMain() {
		super("Main");
		this.setInv(Bukkit.getServer().createInventory(null, 9, "Category Selection"));
	}

	@Override
	public void showInventory(Player p) {
		createItem(Material.COMPASS, getInv(), 0, "World Commands", "Commands that effect the world.");
		createItem(Material.SKULL_ITEM, getInv(), 4, "Player Commands", "Commands that effect players.");
		createItem(Material.NETHER_STAR, getInv(), 8, "Server Commands", "Commands that effect the server.");
		
		p.openInventory(getInv());
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void InventoryClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		if(e.getInventory().equals(getInv())){
			e.setCancelled(true);
			if(e.getCurrentItem() == null){
				return;
			} else if(e.getCurrentItem().getType() == Material.COMPASS){
				p.closeInventory();
				GUI guiWorld = getInstance().getGUIManager().getGUI("world");
				guiWorld.showInventory(p);
			} else if(e.getCurrentItem().getType() == Material.SKULL_ITEM){
				p.closeInventory();
				GUI guiWorld = getInstance().getGUIManager().getGUI("player");
				guiWorld.showInventory(p);
			} else if(e.getCurrentItem().getType() == Material.NETHER_STAR){
				p.closeInventory();
				GUI guiWorld = getInstance().getGUIManager().getGUI("server");
				guiWorld.showInventory(p);
			}
		}
	}
}
