package uk.cubed.pi.gui.exe;

import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;

import uk.cubed.pi.core.PI;
import uk.cubed.pi.gui.GUI;

public class GUIAchievements extends GUI {

	public GUIAchievements() {
		super("Achievements");
		this.setInv(Bukkit.createInventory(null, 54, "Achievements"));
	}

	@Override
	public void showInventory(Player p) {
		if (Bukkit.getOnlinePlayers().size() <= 54) {
			for (int i = 0; i < Bukkit.getOnlinePlayers().size() && i < 54; i++) {
				Player v = (Player) Bukkit.getOnlinePlayers().toArray()[i];
				createItem(Material.SKULL_ITEM, getInv(), i, v.getName(), "Give " + v.getName() + " all achievements.");
			}
		} else {
			//TODO Make this shit here. Too layzee atm.
		}
		
		p.openInventory(getInv());
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void InventoryClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		if(e.getInventory().equals(getInv())){
			e.setCancelled(true);
			if(e.getCurrentItem() == null){
				return;
			} else if(e.getCurrentItem().getType() == Material.SKULL_ITEM){
				p.closeInventory();
				Player v = getAnyPlayer(e.getCurrentItem().getItemMeta().getDisplayName());
				if (v == null) {
					PI.sendMessage(p, "Target player not found. Command failed.");
					return;
				}
				for (Achievement a: Achievement.values()) {
					if (!v.hasAchievement(a)) v.awardAchievement(a);
				}
				PI.sendMessage(p, "Awarded " + v.getName() + " all achievements.");
			}
		}
	}
}
