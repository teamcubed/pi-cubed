package uk.cubed.pi.gui.exe;

import java.util.LinkedList;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.command.Command.Category;
import uk.cubed.pi.core.PI;
import uk.cubed.pi.gui.GUI;

public class GUIWorld extends GUI {
	
	public GUIWorld() {
		super("world");
		this.setInv(Bukkit.getServer().createInventory(null, 54, "World Commands"));
	}

	@Override
	public void showInventory(Player p) {
		LinkedList<Command> WorldCMDs = new LinkedList<Command>();
		
		for (Entry<String, Command> entry: PI.getInstance().getCommandManager().getCommandMap().entrySet()) {
			if (entry.getValue().getCommandCategory() == Category.WORLD || entry.getValue().getCommandCategory() == Category.ALL) {
				WorldCMDs.add(entry.getValue());
			}
		}
		
		if (WorldCMDs.size() == 0) {
			PI.sendMessage(p, "No commands to show.");
			return;
		}
		
		if (WorldCMDs.size() <= 54) {
			for (int i = 0; i < WorldCMDs.size(); i++) {
				Command cmd = WorldCMDs.get(i);
				createItem(Material.REDSTONE, getInv(), i, cmd.getCommandName(), cmd.getCommandDescription());
			}
		} else {
			//TODO Make this shit here. Too layzee atm.
		}
		
		p.openInventory(getInv());
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void InventoryClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		if(e.getInventory().equals(getInv())){
			e.setCancelled(true);
			if(e.getCurrentItem() == null){
				return;
			} else if(e.getCurrentItem().getType() == Material.REDSTONE){
				p.closeInventory();
				GUI guiCommand = getInstance().getGUIManager().getGUI(e.getCurrentItem().getItemMeta().getDisplayName().toLowerCase());
				if (guiCommand == null) {
					PI.sendMessage(p, "There is no GUI for this command. Try *" + e.getCurrentItem().getItemMeta().getDisplayName() + ".");
				} else {
					guiCommand.showInventory(p);
				}
			}
		}
	}
}
