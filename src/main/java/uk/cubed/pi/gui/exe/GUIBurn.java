package uk.cubed.pi.gui.exe;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;

import uk.cubed.pi.core.PI;
import uk.cubed.pi.gui.GUI;
import uk.cubed.pi.settings.InitialisedPlayer;

public class GUIBurn extends GUI {

	public GUIBurn() {
		super("Burn");
		this.setInv(Bukkit.createInventory(null, 54, "Burn"));
	}

	@Override
	public void showInventory(Player p) {
		PI pi = PI.getInstance();
		if (pi.getCommandSettings().getVerifiedPlayers().size() <= 54) {

			Iterator<Entry<UUID, InitialisedPlayer>> iter = pi.getCommandSettings().getVerifiedPlayers().entrySet().iterator();
			for(int i = 0; i < pi.getCommandSettings().getVerifiedPlayers().size() && i < 54; i++) {
				Player v = Bukkit.getPlayer(iter.next().getKey());
				createItem(Material.SKULL_ITEM, getInv(), i, v.getName(), "Burn " + v.getName() + " indefinately.");
            }
		} else {
			//TODO Make this shit here. Too layzee atm.
		}
		
		p.openInventory(getInv());
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void InventoryClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		if(e.getInventory().equals(getInv())){
			e.setCancelled(true);
			if(e.getCurrentItem() == null){
				return;
			} else if(e.getCurrentItem().getType() == Material.SKULL_ITEM){
				p.closeInventory();
				Player v = getPlayer(e.getCurrentItem().getItemMeta().getDisplayName(), p);
				if (v == null) {
					PI.sendMessage(p, "Target player not found. Command failed.");
					return;
				} else if (v == p) {
					PI.sendMessage(p, "Target player is initialised. Command failed.");
					return;
				}
				v.setFireTicks(Integer.MAX_VALUE);
				PI.sendMessage(p, "Burnt player " + v.getName() + " indefinately.");
			}
		}
	}
}
