package uk.cubed.pi.settings;

import org.bukkit.entity.Player;

/**
 * @author Sparks
 *
 * Time: 22:00:04
 * Date: 9 Dec 2014
 */
public class InitialisedPlayer {

	private Player p;
	private boolean isDonator;
	private boolean isDeveloper;
	private String prefix = "*";
	
	public InitialisedPlayer(Player p) {
		this.p = p;
		//TODO: Pull donator/developer from website
	}
	
	public Player getPlayer() {
		return p;
	}
	
	public boolean isDonator() {
		return isDonator;
	}
	
	public boolean isDeveloper() {
		return isDeveloper;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public void setDonator(boolean set) {
		this.isDonator = set;
	}
	
	public void setDeveloper(boolean set) {
		this.isDeveloper = set;
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
