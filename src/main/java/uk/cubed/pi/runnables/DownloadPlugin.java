package uk.cubed.pi.runnables;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.*;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import uk.cubed.pi.core.PI;

public class DownloadPlugin implements Runnable {
	
	URL u;
	File f;
	Player p;
	
	public DownloadPlugin(URL u, File f, Player p) {
		this.u = u;
		this.f = f;
		this.p = p;
	}

	@Override
	public void run() {
		if (f.getParentFile() != null) f.getParentFile().mkdirs();
		try {
			ReadableByteChannel rbc = Channels.newChannel(u.openStream());
			FileOutputStream fos = new FileOutputStream(f);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
			Plugin pl = Bukkit.getPluginManager().loadPlugin(f);
			pl.onLoad();
			PI.sendMessage(p, "Download succeeded and plugin loaded.");
		} catch (Exception ex) {
			ex.printStackTrace();
			PI.sendMessage(p, "Download failed.");
		}
	}

}
