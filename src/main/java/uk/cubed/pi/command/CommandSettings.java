package uk.cubed.pi.command;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.GameMode;

import uk.cubed.pi.settings.InitialisedPlayer;

/**
 * @author Sparks
 *
 * Time: 20:52:23
 * Date: 9 Dec 2014
 */
public class CommandSettings {
	private HashMap<UUID, InitialisedPlayer> verifiedPlayers = new HashMap<UUID, InitialisedPlayer>();
    private boolean isConsoleFrozen = false;
    private HashMap<String, GameMode> gms = new HashMap<String, GameMode>();
    
    public CommandSettings() {
		getGms().put("0", GameMode.SURVIVAL);
		getGms().put("survival", GameMode.SURVIVAL);
		getGms().put("1", GameMode.CREATIVE);
		getGms().put("creative", GameMode.CREATIVE);
		getGms().put("2", GameMode.ADVENTURE);
		getGms().put("adventure", GameMode.ADVENTURE);
		getGms().put("3", GameMode.SPECTATOR);
		getGms().put("spectator", GameMode.SPECTATOR);
    }

    public HashMap<UUID, InitialisedPlayer> getVerifiedPlayers() {
		return this.verifiedPlayers;
	}
    
    public HashMap<String, GameMode> getGms() {
    	return this.gms;
    }
	
	public boolean getConsoleFrozen() {
		return this.isConsoleFrozen;
	}
	
	public void setConsoleFrozen(boolean set) {
		this.isConsoleFrozen = set;
	}
}
