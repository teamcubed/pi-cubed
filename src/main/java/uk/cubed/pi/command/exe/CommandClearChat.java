package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;

import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandClearChat extends Command {

	public CommandClearChat() {
		super("ClearChat");
		this.setCommandDescription("Clears the chat using broadcast.");
		this.setCommandSyntax("{PREFIX}ClearChat");
		this.setCommandCategory(Category.SERVER);
		this.setCommandExample("{PREFIX}ClearChat");
		this.setMaxArgs(0);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		for (int i = 0; i < 120; i++) {
			Bukkit.broadcastMessage(" ");
		}
	}

}
