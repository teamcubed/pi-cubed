package uk.cubed.pi.command.exe;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandTPPos extends Command {

	public CommandTPPos() {
		super("TPPos");
		this.setCommandDescription("TPs to coordinates.");
		this.setCommandSyntax("{PREFIX}TPPos <X> <Y> <Z>");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}TPPos 0 90 0");
		this.setMinArgs(3);
		this.setMaxArgs(3);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		int X;
		int Y;
		int Z;
		if (isInteger(args[0])) {
			X = Integer.valueOf(args[0]);
		} else {
			PI.sendMessage(p, "X must be an integer.");
			return;
		}
		if (isInteger(args[1])) {
			Y = Integer.valueOf(args[1]);
		} else {
			PI.sendMessage(p, "Y must be an integer.");
			return;
		}
		if (isInteger(args[2])) {
			Z = Integer.valueOf(args[2]);
		} else {
			PI.sendMessage(p, "Z must be an integer.");
			return;
		}
		Location loc = new Location(p.getWorld(), X, Y, Z);
		p.teleport(loc);
		PI.sendMessage(p, "Teleported to: X:" + String.valueOf(loc.getX()) + ", Y:" + String.valueOf(loc.getY()) + ", Z:" + String.valueOf(loc.getZ()) + ".");
	}
	
	boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}
