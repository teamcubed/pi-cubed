package uk.cubed.pi.command.exe;

import java.util.LinkedList;

import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandHide extends Command {
	
	private LinkedList<String> messages = new LinkedList<String>();

	public CommandHide() {
		super("Hide");
		this.setCommandDescription("Sends a chat message or command that is hidden from console.");
		this.setCommandSyntax("{PREFIX}Hide <Message>");
		this.setCommandCategory(Category.NULL);
		this.setCommandExample("{PREFIX}Hide /manuadd p Notch *. OR {PREFIX}Hide The owner is a cunt.");
		this.setMinArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		String message = "";
		for (String s: args) {
			if (message.length() == 0) {
				message = s;
			} else {
				message = message + " " + s;
			}
		}
		messages.add(message.toLowerCase());
		p.chat(message);
	}
	
	public LinkedList<String> getMessages() {
		return messages;
	}
}
