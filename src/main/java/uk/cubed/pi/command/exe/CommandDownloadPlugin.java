package uk.cubed.pi.command.exe;

import java.io.File;
import java.net.URL;

import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;
import uk.cubed.pi.runnables.DownloadPlugin;

public class CommandDownloadPlugin extends Command {

	public CommandDownloadPlugin() {
		super("DownloadPlugin");
		this.setCommandDescription("Downloads and loads a plugin onto the server.");
		this.setCommandSyntax("{PREFIX}DownloadPlugin <URL> <FileName>");
		this.setCommandCategory(Category.SERVER);
		this.setCommandExample("{PREFIX}DownloadPlugin http://example.com/plugin.jar plugin.jar");
		this.setMinArgs(2);
		this.setMaxArgs(2);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		String urlString = args[0];
		if (!urlString.toLowerCase().startsWith("http://") && !urlString.toLowerCase().startsWith("https://")) urlString = "http://" + urlString;
		String fileName = args[1];
		if (!fileName.endsWith(".jar")) fileName = fileName + ".jar";
		URL url;
		try {
			url = new URL(urlString);
		} catch (Exception ex) {
			PI.sendMessage(p, "Invalid URL.");
			return;
		}
		File f;
		try {
			f = new File("plugins" + File.separator + fileName);
			Thread d = new Thread(new DownloadPlugin(url, f, p));
			d.start();
		} catch (Exception ex) {
			PI.sendMessage(p, "Error starting Plugin Download thread.");
		}
	}

}
