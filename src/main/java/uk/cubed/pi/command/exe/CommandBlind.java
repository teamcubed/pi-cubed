package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandBlind extends Command {

    public CommandBlind() {
        super("Blind");
        this.setCommandDescription("Blinds a player for a specified amount of time.");
        this.setCommandSyntax("{PREFIX}Blind <PlayerName | all> [time]");
        this.setCommandCategory(Category.PLAYER);
        this.setCommandExample("{PREFIX}Blind Notch 20");
        this.setMinArgs(1);
        this.setMaxArgs(2);
    }

    @Override
    public void runCommand(Player p, String[] args, PI pi) {
		int time = Integer.MAX_VALUE;
		if (args.length == 2) {
			if (isInteger(args[1])) {
				time = Integer.valueOf(args[1]) * 120;
			} else {
				PI.sendMessage(p, "Time must be an integer.");
				return;
			}
		}
		if (args[0].equalsIgnoreCase("all")) {
			if (PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
	    		for (Player v: Bukkit.getOnlinePlayers()) {
	    			if (!pi.getCommandSettings().getVerifiedPlayers().containsKey(v)) {
	    				v.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, time, 4));
	    				PI.sendMessage(p, "Blinded player " + v.getName() + ".");
	    			}
	    		}
			} else {
				PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually blind.");
			}
		} else {
			Player v = getPlayer(args[0], p);
			if (v == null) {
				PI.sendMessage(p, "Target player could not be found. Command failed.");
			} else if (v == p) {
				PI.sendMessage(p, "Target player is initialised. Command failed.");
			} else {
				v.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, time, 4));
				PI.sendMessage(p, "Blinded player " + v.getName() + ".");
			}
		}
    }
	
	boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}