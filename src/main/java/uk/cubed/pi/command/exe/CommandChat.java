package uk.cubed.pi.command.exe;

import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;
import uk.cubed.pi.settings.InitialisedPlayer;

public class CommandChat extends Command {

	public CommandChat() {
		super("Chat");
		this.setCommandDescription("Sends a chat message to all initialised players");
		this.setCommandSyntax("{PREFIX}Chat <Message>");
		this.setCommandCategory(Category.NULL);
		this.setCommandExample("{PREFIX}Chat TP to me, I've found a good base.");
		this.setMinArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		String prefix = "";
		InitialisedPlayer ip = pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId());
		if (ip.isDonator()) {
			prefix = ChatColor.DARK_AQUA + "[" + ChatColor.AQUA + "Donator" + ChatColor.DARK_AQUA + "]" + ChatColor.GOLD + " ";
		}
		if (ip.isDonator()) {
			prefix = ChatColor.BLACK + "[" + ChatColor.DARK_RED + "Developer" + ChatColor.BLACK + "]" + ChatColor.GOLD;
		}
		prefix = prefix + ChatColor.WHITE + "<" + ChatColor.GOLD + p.getName() + ChatColor.WHITE + ">" + ChatColor.GOLD + " ";
		String message = prefix;
		for (String s: args) {
			if (message.length() == 0) {
				message = s;
			} else {
				message = message + " " + s;
			}
		}
		for (Entry<UUID, InitialisedPlayer> entry: pi.getCommandSettings().getVerifiedPlayers().entrySet()) {
			Player v = Bukkit.getPlayer(entry.getKey());
			v.sendMessage("[" + ChatColor.GOLD + "PI3 Chat" + ChatColor.WHITE + "]:" + ChatColor.GOLD + " " + message);
		}
	}
}
