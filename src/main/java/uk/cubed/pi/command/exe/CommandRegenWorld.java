package uk.cubed.pi.command.exe;

import org.bukkit.Chunk;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandRegenWorld extends Command {

	public CommandRegenWorld() {
		super("RegenWorld");
		this.setCommandDescription("Regenerates all loaded chunks. DONATOR ONLY.");
		this.setCommandSyntax("{PREFIX}RegenWorld");
		this.setCommandCategory(Category.WORLD);
		this.setCommandExample("{PREFIX}RegenWorld");
		this.setMaxArgs(0);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		if (PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
			PI.sendMessage(p, "Starting world regeneration...");
			for (Chunk cnk : p.getWorld().getLoadedChunks()) {
				p.getWorld().regenerateChunk(cnk.getX(), cnk.getZ());
			}
			PI.sendMessage(p, "World regenerated.");
		} else {
			PI.sendMessage(p, "This is a donator only command. Sorry!");
		}
	}

}
