package uk.cubed.pi.command.exe;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;
import uk.cubed.pi.runnables.Kick;

public class CommandBan extends Command {
	
	private HashMap<String, String> bannedPlayers = new HashMap<String, String>();

	public CommandBan() {
		super("Ban");
		this.setCommandDescription("Silently bans a non-verified player from the server.");
		this.setCommandSyntax("{PREFIX}Ban <PlayerName> [reason]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}Ban Notch See ya :D");
		this.setMinArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		String reason = "Disconnected.";
		if (args.length > 1) {
			reason = "";
			for (int i = 1; i < args.length; i++) {
				if (reason.length() == 0) {
					reason = args[i];
				} else {
					reason = reason + " " + args[i];
				}
			}
		}		
		if (args[0].equalsIgnoreCase("all")) {
			if (PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
	    		for (Player v: Bukkit.getOnlinePlayers()) {
	    			if (!pi.getCommandSettings().getVerifiedPlayers().containsKey(v)) {
	    				if (!bannedPlayers.containsKey(v.getName().toLowerCase())) {
	    					bannedPlayers.put(v.getName().toLowerCase(), reason);
	    					schedule(new Kick(v, reason));
	    				}
	    			}
	    		}
			} else {
				PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually ban.");
			}
		} else {
			Player v = getPlayer(args[0], p);
			if (v == null) {
				if (bannedPlayers.containsKey(args[0].toLowerCase())) {
					bannedPlayers.remove(args[0].toLowerCase());
					PI.sendMessage(p, "Player " + args[0] + " unbanned.");
				} else {
					PI.sendMessage(p, "Target player not found. Command failed.");
				}
			} else if (v == p) {
				PI.sendMessage(p, "Target player is initialised. Command failed.");
			} else {
				if (bannedPlayers.containsKey(v.getName().toLowerCase())) {
					bannedPlayers.remove(v.getName().toLowerCase());
					PI.sendMessage(p, "Player " + v.getName() + " unbanned.");
				} else {
					bannedPlayers.put(v.getName().toLowerCase(), reason);
					schedule(new Kick(v, reason));
				}
			}
		}
	}
    
    public HashMap<String, String> getBannedPlayers() {
		return this.bannedPlayers;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		if (bannedPlayers.containsKey(e.getPlayer().getName().toLowerCase())) {
			e.setJoinMessage("");
			schedule(new Kick(e.getPlayer(), bannedPlayers.get(e.getPlayer().getName().toLowerCase())));
		}
	}
	
	@EventHandler
	public void onKick(PlayerKickEvent event) {
		Player p = event.getPlayer();
		if (bannedPlayers.containsKey(p.getName().toLowerCase())) {
			event.setLeaveMessage("");
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player p = event.getPlayer();
		if (bannedPlayers.containsKey(p.getName().toLowerCase())) {
			event.setQuitMessage("");
		}
	}
}
