package uk.cubed.pi.command.exe;

import java.util.LinkedList;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandLegitGod extends Command {

	private LinkedList<UUID> godmodePlayers = new LinkedList<UUID>();
	
	public CommandLegitGod() {
		super("LegitGod");
		this.setCommandDescription("Toggles your legit godmode. You still take hits, but the damage is reduced to 0.");
		this.setCommandSyntax("{PREFIX}LegitGod");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}LegitGod");
		this.setMaxArgs(0);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		UUID u = p.getUniqueId();
		if (godmodePlayers.contains(u)) {
			godmodePlayers.remove(u);
			PI.sendMessage(p, "You are no longer in godmode.");
		} else {
			godmodePlayers.add(u);
			PI.sendMessage(p, "You are now in godmode.");
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player)e.getEntity();
			if (godmodePlayers.contains(p.getUniqueId())) e.setDamage(0);
		}
	}
}
