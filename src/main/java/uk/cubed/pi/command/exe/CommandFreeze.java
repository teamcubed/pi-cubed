package uk.cubed.pi.command.exe;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.server.ServerCommandEvent;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandFreeze extends Command {

	public CommandFreeze() {
		super("FreezeCMD");
		this.setCommandDescription("Toggles all console input and output.");
		this.setCommandSyntax("{PREFIX}FreezeCMD");
		this.setCommandCategory(Category.SERVER);
		this.setCommandExample("{PREFIX}FreezeCMD");
		this.setMaxArgs(0);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		setCommandState(!getCommandState());
		if (getCommandState()) {
			PI.sendMessage(p, "Server console is now frozen.");
		} else {
			PI.sendMessage(p, "Server console is no longer frozen.");
		}
	}
	
	@EventHandler
	public void onServerCommand(ServerCommandEvent e) {
		if (getCommandState()) e.setCommand("");
	}
}
