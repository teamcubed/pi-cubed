package uk.cubed.pi.command.exe;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandTPBed extends Command {

	public CommandTPBed() {
		super("TPBed");
		this.setCommandDescription("TPs to a player's bed.");
		this.setCommandSyntax("{PREFIX}TPBed <PlayerName>");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}TPBed Notch");
		this.setMinArgs(1);
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		Player v = getAnyPlayer(args[0]);
		if (v.equals(null)) {
			PI.sendMessage(p, "Target player not found. Command failed.");
		} else {
			Location loc = v.getBedSpawnLocation();
			if (loc == null) {
				PI.sendMessage(p, "Player does not have a bed spawn.");
			}
			p.teleport(loc);
			PI.sendMessage(p, "Teleported to: X:" + String.valueOf(loc.getX()) + ", Y:" + String.valueOf(loc.getY()) + ", Z:" + String.valueOf(loc.getZ()) + ".");	
		}
	}
	
	boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}
