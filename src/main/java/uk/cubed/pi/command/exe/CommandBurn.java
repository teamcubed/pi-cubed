package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandBurn extends Command {
	
	public CommandBurn() {
		super("Burn");
		this.setCommandDescription("Burns one or all players.");
		this.setCommandSyntax("{PREFIX}Burn <PlayerName> [time]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}Burn Notch 20");
		this.setMinArgs(1);
		this.setMaxArgs(2);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		int time = Integer.MAX_VALUE;
		if (args.length == 2) {
			if (isInteger(args[1])) {
				time = Integer.valueOf(args[1]) * 120;
			} else {
				PI.sendMessage(p, "Time must be an integer.");
				return;
			}
		}
		if (args[0].equalsIgnoreCase("all")) {
			if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
				for (Player v: Bukkit.getOnlinePlayers()) {
					if (pi.getCommandSettings().getVerifiedPlayers().containsKey(v)) {
						PI.sendMessage(p, "Cannot burn " + v.getName() + "; verified player.");
					} else {
						v.setFireTicks(time);
						PI.sendMessage(p, "Burnt player " + v.getName() + ".");
					}
				}
			} else {
				PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually burn players.");
			}
		} else {
			Player v = getAnyPlayer(args[0]);
			if (v == null) {
				PI.sendMessage(p, "Target player could not be found. Command failed.");
			} else if (v == p) {
				PI.sendMessage(p, "Target player is initialised. Command failed.");
			} else {
				v.setFireTicks(time);
				PI.sendMessage(p, "Burnt player " + v.getName() + ".");
			}
		}
	}
	
	boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

}
