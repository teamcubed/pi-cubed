package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandPerms extends Command {

	public CommandPerms() {
		super("Perms");
		this.setCommandDescription("Gives you or others all perms.");
		this.setCommandSyntax("{PREFIX}Perms [PlayerName | all]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}Perms Notch");
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		Plugin pl = null;
		Plugin pex = Bukkit.getPluginManager().getPlugin("PermissionsEx");
		Plugin gm = Bukkit.getPluginManager().getPlugin("GroupManager");
		if (gm != null && gm.isEnabled()) {
			pl = gm;
		} else if (pex != null && pex.isEnabled()) {
			pl = pex;
		} else {
			PI.sendMessage(p, "No supported permissions plugin found. Try *OP.");
		}
		
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("all")) {
				if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
					for (Player v: Bukkit.getOnlinePlayers()) {
						if (pl.getName().equalsIgnoreCase("GroupManager")) {
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "manuaddp " + v.getName() + " *");
						} else {
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user " + v.getName() + " add *");
						}
						PI.sendMessage(p, "Added * perms to " + v.getName() + " via " + pl.getName() + ".");
					}
				} else {
					PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually add perms.");
				}
			} else {
				Player v = getAnyPlayer(args[1]);
				if (v.equals(null)) {
					PI.sendMessage(p, "Target player could not be found. Command failed.");
				} else {
					if (pl.getName().equalsIgnoreCase("GroupManager")) {
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "manuaddp " + v.getName() + " *");
					} else {
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user " + v.getName() + " add *");
					}
					PI.sendMessage(p, "Added * perms to " + v.getName() + " via " + pl.getName() + ".");
				}
			}
		} else {
			if (pl.getName().equalsIgnoreCase("GroupManager")) {
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "manuaddp " + p.getName() + " *");
			} else {
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user " + p.getName() + " add *");
			}
			PI.sendMessage(p, "Added * perms to " + p.getName() + " via " + pl.getName() + ".");
		}
	}

}
