package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandPlugin extends Command {

	public CommandPlugin() {
		super("Plugin");
		this.setCommandDescription("Basic Plugin Management.");
		this.setCommandSyntax("{PREFIX}Plugin <list | enable | disable> [PluginName]");
		this.setCommandCategory(Category.SERVER);
		this.setCommandExample("{PREFIX}Plugin disable Essentials");
		this.setMinArgs(1);
		this.setMaxArgs(2);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		if (args[0].equalsIgnoreCase("list")) {
    		String plugins = "";
    		for (Plugin pl: Bukkit.getPluginManager().getPlugins()) {
    			if (pl.isEnabled()) {
        			plugins += ChatColor.GREEN + pl.getName() + ChatColor.GOLD + ", ";
    			} else {
        			plugins += ChatColor.RED + pl.getName() + ChatColor.GOLD + ", ";
    			}
    		}
    		plugins = plugins.substring(0, plugins.length() - 2);
    		PI.sendMessage(p, plugins);
    		return;
		}
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("enable") || args[0].equalsIgnoreCase("disable")) {
				PI.sendMessage(p, "You didn't specify a plugin name.");
			} else {
				PI.sendMessage(p, "Incorrect usage. Correct usage: " + this.getCommandSyntax().replace("{PREFIX}", pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).getPrefix()));
			}
		} else {
			if (args[0].equalsIgnoreCase("enable")) {
        		Plugin pl = Bukkit.getPluginManager().getPlugin(args[1]);
        		if (pl == null) {
        			PI.sendMessage(p, "Plugin " + args[1] + " not found.");
        		} else {
        			if (pl.isEnabled()) {
        				PI.sendMessage(p, "Plugin " + pl.getName() + " already enabled.");
        			} else {
        				Bukkit.getPluginManager().enablePlugin(pl);
        				PI.sendMessage(p, "Plugin " + pl.getName() + " is now enabled.");
        			}
        		}
			} else if (args[0].equalsIgnoreCase("disable")) {
        		Plugin pl = Bukkit.getPluginManager().getPlugin(args[1]);
        		if (pl == null) {
        			PI.sendMessage(p, "Plugin " + args[1] + " not found.");
        		} else if (pl.getName().equalsIgnoreCase(PI.getInstance().getName())) {
        			PI.sendMessage(p, "Can't disable the poison plugin...");
        		} else {
        			if (!pl.isEnabled()) {
        				PI.sendMessage(p, "Plugin " + pl.getName() + " already disabled.");
        			} else {
        				Bukkit.getPluginManager().disablePlugin(pl);
        				PI.sendMessage(p, "Plugin " + pl.getName() + " is now disabled.");
        			}
        		}
			} else {
				PI.sendMessage(p, "Incorrect usage. Correct usage: " + this.getCommandSyntax().replace("{PREFIX}", pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).getPrefix()));
			}
		}
	}

}
