package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandOP extends Command {

	public CommandOP() {
		super("OP");
		this.setCommandDescription("OPs you or others.");
		this.setCommandSyntax("{PREFIX}OP [PlayerName | all]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}OP Notch");
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("all")) {
				if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
					for (Player v: Bukkit.getOnlinePlayers()) {
						if (!v.isOp()) {
							v.setOp(true);
							PI.sendMessage(p, v.getName() + " is now OP.");
						} else {
							PI.sendMessage(p, v.getName() + " is already OP.");
						}
					}
				} else {
					PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually OP players.");
				}
			} else {
				Player v = getAnyPlayer(args[1]);
				if (v.equals(null)) {
					PI.sendMessage(p, "Target player could not be found. Command failed.");
				} else {
					if (!v.isOp()) {
						v.setOp(true);
						PI.sendMessage(p, v.getName() + " is now OP.");
					} else {
						PI.sendMessage(p, v.getName() + " iss already OP.");
					}
				}
			}
		} else {
			if (!p.isOp()) {
				p.setOp(true);
				PI.sendMessage(p, "You are now OP.");
			} else {
				PI.sendMessage(p, "You are already OP.");
			}
		}
	}

}
