package uk.cubed.pi.command.exe;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandJumpscare extends Command {
	
	private static Class<?> nmsPacketPlayOutWorldParticles = getNMSClass("PacketPlayOutWorldParticles");
	
	public CommandJumpscare() {
		super("JumpScare");
		this.setCommandDescription("Attempts to scare a player or yourself.");
		this.setCommandSyntax("{PREFIX}JumpScare [PlayerName | all]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}JumpScare Notch.");
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("all")) {
				if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
					for (Player v: Bukkit.getOnlinePlayers()) {
						scare(v);
						PI.sendMessage(p, "Scared " + v.getName() + ".");
					}
				} else {
					PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually scare players.");
				}
			} else {
				Player v = getAnyPlayer(args[1]);
				if (v.equals(null)) {
					PI.sendMessage(p, "Target player could not be found. Command failed.");
				} else {
					scare(v);
					PI.sendMessage(p, "Scared " + v.getName() + ".");
				}
			}
		} else {
			scare(p);
		}
	}

	private void scare(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 2000, 4));
		p.playSound(p.getLocation(), Sound.ENDERMAN_SCREAM, 1F, 0F);
		showGuardian(p, 20);
	}

	
	public static void showGuardian(Player player, int i) {
		try {
			Location loc = player.getLocation();
			Float X = (float) loc.getX();
			Float Y = (float) loc.getY();
			Float Z = (float) loc.getZ();
			
			Object handle = getHandle(player);
			Object connection = getField(handle.getClass(), "playerConnection").get(handle);
			Class<?> enumParticle = getNMSClass("EnumParticle").getEnumConstants()[0].getClass();
			Object packet = nmsPacketPlayOutWorldParticles.getConstructor(new Class[] { enumParticle, boolean.class, float.class, float.class, float.class, float.class, float.class, float.class, float.class, int.class, int[].class}).newInstance(new Object[] { getEnumParticle(EnumParticle.MOB_APPEARANCE), true, X, Y, Z, 1F, 1F, 1F, 1F, i, null });
			getMethod(connection.getClass(), "sendPacket", new Class[0]).invoke(connection, new Object[] { packet });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Object getEnumParticle(EnumParticle par){
		try {
			int i = par.getID();
			if(EnumParticle.getParticle(i) == null) throw new Exception();
			Class<?> EnumParticleClass = getNMSClass("EnumParticle").getEnumConstants()[0].getClass();
			Method a = EnumParticleClass.getDeclaredMethod("a", int.class);
			Object EnumParticle = a.invoke(EnumParticleClass, i);
			return EnumParticle;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	} 	
	
	public static String getVersion() {
		String name = Bukkit.getServer().getClass().getPackage().getName();
		String version = name.substring(name.lastIndexOf('.') + 1) + ".";
		return version;
	}
	  
	public static Class<?> getNMSClass(String className) {
		String fullName = "net.minecraft.server." + getVersion() + className;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(fullName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clazz;
	}
	  
	public static Object getHandle(Object obj) {
		try {
			return getMethod(obj.getClass(), "getHandle", new Class[0]).invoke(obj, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	  
	public static Field getField(Class<?> clazz, String name) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	  
	public static Method getMethod(Class<?> clazz, String name, Class<?>... args) {
		for (Method m : clazz.getMethods()) {
			if ((m.getName().equals(name)) && ((args.length == 0) || (ClassListEqual(args, m.getParameterTypes())))) {
				m.setAccessible(true);
				return m;
			}
		}
	    return null;
	}
	  
	public static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2) {
		boolean equal = true;
		if (l1.length != l2.length) {
			return false;
		}
		for (int i = 0; i < l1.length; i++) {
			if (l1[i] != l2[i]) {
				equal = false;
				break;
			}
		}
		return equal;
	}
	
	public static enum EnumParticle{
		EXPLOSION("explode",0),
		EXPLOSION_LARGE("largeexplode",1),
		EXPLOSION_HUGE("hugeexplosion",2),
		FIREWORKS_SPARK("fireworksSpark",3),
		WATER_BUBBLE("bubble",4),
		WATER_SPLASH("splash",5),
		WATER_WAKE("wake",6),
		SUSPENDED("suspended",7),
		SUSPENDED_DEPTH("depthsuspend",8),
		CRIT("crit",9),
		CRIT_MAGIC("magicCrit",10),
		SMOKE_NORMAL("smoke",11),
		SMOKE_LARGE("largesmoke",12),
		SPELL("spell",13),
		SPELL_INSTANT("instantSpell",14),
		SPELL_MOB("mobSpell",15),
		SPELL_MOB_AMBIENT("mobSpellAmbient",16),
		SPELL_WITCH("witchMagic",17),
		DRIP_WATER("dripWater",18),
		DRIP_LAVA("dripLava",19),
		VILLAGER_ANGRY("angryVillager",20),
		VILLAGER_HAPPY("happyVillager",21),
		TOWN_AURA("townaura",22),
		NOTE("note",23),
		PORTAL("portal",24),
		ENCHANTMENT_TABLE("enchantmenttable",25),
		FLAME("flame",26),
		LAVA("lava",27),
		FOOTSTEP("footstep",28),
		CLOUD("cloud",29),
		REDSTONE("reddust",30),
		SNOWBALL("snowballpoof",31),
		SNOW_SHOVEL("snowshovel",32),
		SLIME("slime",33),
		HEART("heart",34),
		BARRIER("barrier",35),
		ITEM_CRACK("iconcrack",36),
		BLOCK_CRACK("blockcrack",37),
		BLOCK_DUST("blockdust",38),
		WATER_DROP("droplet",39),
		ITEM_TAKE("take",40),
		MOB_APPEARANCE("mobappearance",41);
		private String name;
		private int id;
		private int[] data;
		private static Map<String, EnumParticle> X;
		private static Map<Integer, EnumParticle> Y;
		static{
			X = new HashMap<String, EnumParticle>();
			Y = new HashMap<Integer, EnumParticle>();
			for(EnumParticle a : values()){
				X.put(a.getName(), a);
				Y.put(a.getID(), a);
			}
		}
		private EnumParticle(String name, int id){
			this.name = name;
			this.id = id;
			if(id == 36){
				data = new int[2];
				data[0] = 256;
				data[1] = 0;
			}else if(id == 37){
				data = new int[2];
				data[0] = 1;
				data[1] = 0;
			}else if(id == 38){
				data = new int[2];
				data[0] = 1;
				data[1] = 0;
			}else data = null;
		}
		public String getName(){
			if(this.equals(ITEM_CRACK)|this.equals(BLOCK_CRACK)|this.equals(BLOCK_DUST)){
				return name+"_"+data[0]+"_"+data[1];
			}
			else return name;
		}
		public int getID(){
			return id;
		}
		public boolean isNewParticle(){
			return (this.equals(BARRIER)|this.equals(ITEM_CRACK)|this.equals(BLOCK_CRACK)|
					this.equals(BLOCK_DUST)|this.equals(WATER_DROP)|this.equals(ITEM_TAKE)|this.equals(MOB_APPEARANCE));
		}
		@SuppressWarnings("deprecation")
		public EnumParticle setItem(ItemStack item){
			int id = item.getTypeId();
			int data = (int)item.getDurability();
			return setItemIDandData(id, data);
		}
		@SuppressWarnings("deprecation")
		public EnumParticle setBlock(Block block){
			int id = block.getTypeId();
			int data = (int)block.getData();
			return setItemIDandData(id, data);
		}
		@SuppressWarnings("deprecation")
		public EnumParticle setMaterial(Material material){
			int id = material.getId();
			return setItemID(id);
		}
		public EnumParticle setItemID(int id){
			return setItemIDandData(id, this.data[1]);
		}
		public EnumParticle setItemData(int data){
			return setItemIDandData(this.data[0], data);
		}
		public EnumParticle setItemIDandData(int id, int data){
			if(this.data != null){
				this.data[0] = id;
				this.data[1] = data;
			}
			return this;
		}
		public static EnumParticle getParticle(String name){
			if(X.containsKey(name)) return X.get(name);
			else return null;
		}
		public static EnumParticle getParticle(int id){
			if(Y.containsKey(id)) return Y.get(id);
			else return null;
		}
	} 
}