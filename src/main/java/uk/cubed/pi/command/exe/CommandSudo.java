package uk.cubed.pi.command.exe;

import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandSudo extends Command {

	public CommandSudo() {
		super("Sudo");
		this.setCommandDescription("Sends a message or runs a command as another player.");
		this.setCommandSyntax("{PREFIX}Sudo <PlayerName> <message>");
		this.setCommandCategory(Category.NULL);
		this.setCommandExample("{PREFIX}Sudo Notch I'm a fag.");
		this.setMinArgs(2);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		Player v = getAnyPlayer(args[0]);
		if (v == null) {
			PI.sendMessage(p, "Target player could not be found. Command failed.");
		} else if (v == p) {
			PI.sendMessage(p, "Target player is initialised. Command failed.");
		} else {
			String message = "";
			for (int i = 1; i < args.length; i++) {
				if (message.length() == 0) {
					message = args[i];
				} else {
					message = message + " " + args[i];
				}
			}
			PI.sendMessage(p, v.getName() + " now sending " + message + ".");
			v.chat(message);
		}
	}

}
