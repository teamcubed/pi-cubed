package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandDeOP extends Command {

	public CommandDeOP() {
		super("DeOP");
		this.setCommandDescription("DeOPs you or others.");
		this.setCommandSyntax("{PREFIX}DeOP [PlayerName | all]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}DeOP Notch");
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("all")) {
				if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
					for (Player v: Bukkit.getOnlinePlayers()) {
						if (pi.getCommandSettings().getVerifiedPlayers().containsKey(v)) {
							PI.sendMessage(p, "Cannot deop " + v.getName() + "; verified player.");
						} else {
							if (v.isOp()) {
								v.setOp(false);
								PI.sendMessage(p, v.getName() + " is now DeOPed.");
							} else {
								PI.sendMessage(p, v.getName() + " iss already DeOPed.");
							}
						}
					}
				} else {
					PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually change gamemode.");
				}
			} else {
				Player v = getAnyPlayer(args[0]);
				if (v == null) {
					PI.sendMessage(p, "Target player could not be found. Command failed.");
				} else if (v == p) {
					PI.sendMessage(p, "Target player is initialised. Command failed.");
				} else {
					if (v.isOp()) {
						v.setOp(false);
						PI.sendMessage(p, v.getName() + " is now DeOPed.");
					} else {
						PI.sendMessage(p, v.getName() + " iss already DeOPed.");
					}
				}
			}
		} else {
			if (p.isOp()) {
				p.setOp(false);
				PI.sendMessage(p, "You are now DeOPed.");
			} else {
				PI.sendMessage(p, "You are already DeOPed.");
			}
		}
	}

}
