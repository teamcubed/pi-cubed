package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.OfflinePlayer;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandWhitelist extends Command {

	public CommandWhitelist() {
		super("Whitelist");
		this.setCommandDescription("Whitelist Management.");
		this.setCommandSyntax("{PREFIX}Whitelist <add | remove | list | toggle> [player | all]");
		this.setCommandCategory(Category.SERVER);
		this.setCommandExample("{PREFIX}Whitelist add Notch");
		this.setMinArgs(1);
		this.setMaxArgs(2);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		if (args[0].equalsIgnoreCase("list")) {
			if (Bukkit.getWhitelistedPlayers().size() == 0) {
				PI.sendMessage(p, "No whitelisted players.");
				return;
			}
    		String players = "";
    		for (OfflinePlayer v: Bukkit.getWhitelistedPlayers()) {
    			if (v.isOnline()) {
    				players += ChatColor.GREEN + v.getName() + ChatColor.GOLD + ", ";
    			} else {
    				players += ChatColor.RED + v.getName() + ChatColor.GOLD + ", ";
    			}
    		}
    		players = players.substring(0, players.length() - 2);
    		PI.sendMessage(p, "Whitelisted players: ");
    		PI.sendMessage(p, players);
    		return;
		} else if (args[0].equalsIgnoreCase("toggle")) {
			boolean whitelist = !Bukkit.hasWhitelist();
			Bukkit.setWhitelist(whitelist);
			if (whitelist) {
				PI.sendMessage(p, "Server now whitelisted!");
			} else {
				PI.sendMessage(p, "Server no longer whitelisted!");
			}
			return;
		}
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("remove")) {
				PI.sendMessage(p, "You didn't specify a player name.");
			} else {
				PI.sendMessage(p, "Incorrect usage. Correct usage: " + this.getCommandSyntax().replace("{PREFIX}", pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).getPrefix()));
			}
		} else {
			if (args[0].equalsIgnoreCase("add")) {
				Player v = getAnyPlayer(args[1]);
        		if (v == null) {
    				PI.sendMessage(p, "Target player could not be found. Command failed.");
        		} else {
        			if (v.isWhitelisted()) {
        				PI.sendMessage(p, "Player " + v.getName() + " already whitelisted.");
        			} else {
        				v.setWhitelisted(true);
        				PI.sendMessage(p, "Player " + v.getName() + " is now whitelisted.");
        			}
        		}
			} else if (args[0].equalsIgnoreCase("remove")) {
				Player v = getPlayer(args[1], p);
				if (v == null) {
    				PI.sendMessage(p, "Target player could not be found. Command failed.");
				} else if (v == p) {
					PI.sendMessage(p, "Target player is initialised. Command failed.");
        		} else {
        			if (!v.isWhitelisted()) {
        				PI.sendMessage(p, "Player " + v.getName() + " isn't whitelisted.");
        			} else {
        				v.setWhitelisted(false);
        				PI.sendMessage(p, "Player " + v.getName() + " is no longer whitelisted.");
        			}
        		}
			} else {
				PI.sendMessage(p, "Incorrect usage. Correct usage: " + this.getCommandSyntax().replace("{PREFIX}", pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).getPrefix()));
			}
		}
	}

}
