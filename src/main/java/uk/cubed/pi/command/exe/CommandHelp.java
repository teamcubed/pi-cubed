package uk.cubed.pi.command.exe;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandHelp extends Command {

	private static Class<?> nmsChatSerializer = getNMSClass("ChatSerializer");
	private static Class<?> nmsPacketPlayOutChat = getNMSClass("PacketPlayOutChat");
	
	public CommandHelp() {
		super("Help");
		this.setCommandDescription("Shows the help pages.");
		this.setCommandSyntax("{PREFIX}Help [page | CommandName]");
		this.setCommandCategory(Category.ALL);
		this.setCommandExample("{PREFIX}Help DownloadPlugin");
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		TreeMap<String, Command> cmds = pi.getCommandManager().getCommandMap();
		int max = getMaxHelpPages(cmds);
		if (args.length == 0) {
			PI.sendMessage(p, ChatColor.AQUA + "Click a command to insert into chat bar. Hover to see an example.");
			PI.sendMessage(p, "========PI3 Help Menu (1 / " + String.valueOf(max) + ")========");
			Iterator<Entry<String, Command>> iter = cmds.entrySet().iterator();
			for(int i = 0; i < cmds.size() && i < 7; i++) {
				Command cmd = iter.next().getValue();
				sendCommandHelp(p, cmd);
                //PI.sendMessage(p, cmd.getCommandSyntax() + " - " + cmd.getCommandDescription());
            }
			if (max > 1) {
				showNextPage(p, 2);
			}
		} else if (args.length == 1) {
			if (cmds.containsKey(args[0].toLowerCase())) {
				Command cmd = cmds.get(args[0].toLowerCase());
				PI.sendMessage(p, "-------" + cmd.getCommandName() + " Help-------");
				PI.sendMessage(p, "Command: " + cmd.getCommandName());
				PI.sendMessage(p, "Description: " + cmd.getCommandDescription());
				PI.sendMessage(p, "Usage: " + cmd.getCommandSyntax());
				PI.sendMessage(p, "Example: " + cmd.getCommandExample());
				return;
			} else if (isInteger(args[0])) {
				int page = Integer.valueOf(args[0]);
				if (page < 1) {
					PI.sendMessage(p, "Page number must be 1 or greater.");
				} else if (page > max) {
					if (max == 1) {
						PI.sendMessage(p, "There is only 1 help page.");
					} else {
						PI.sendMessage(p, "There are only " + String.valueOf(getMaxHelpPages(cmds)) + " help pages.");
					}
				} else {
	                int startValue = (page - 1) * 7;
	                int endValue = page * 7;
	    			Iterator<Entry<String, Command>> iter = cmds.entrySet().iterator();
	    			for (int i = 0; i < startValue; i++) {
	    				iter.next();
	    			}
	    			PI.sendMessage(p, ChatColor.AQUA + "Click a command to insert into chat bar. Hover to see an example.");
	                PI.sendMessage(p, "========PI3 Help Menu (" + String.valueOf(page) + "/ " + String.valueOf(max) + ")========");
	                for(int i = startValue; i < cmds.size() && i < endValue; i++) {
	                	Command cmd = iter.next().getValue();
	                	sendCommandHelp(p, cmd);
	                    //PI.sendMessage(p, cmd.getCommandSyntax() + " - " + cmd.getCommandDescription());
	                }
	    			if (max > page) {
	    				showNextPage(p, page + 1);
	    			}
				}
			} else {
				PI.sendMessage(p, "Command not found.");
			}
		}
	}

	boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    int getMaxHelpPages(TreeMap<String, Command> list) {
    	if (list.size() % 7 == 0) {
    		return list.size() / 7;
    	} else {
    		return list.size() / 7 + 1;
    	}
    }
    
    void showNextPage(Player p, int i) {
    	String msg = "{\"text\":\"\",\"extra\":[{\"text\":\"[\",\"color\":\"white\"},{\"text\":\"PI3\",\"color\":\"gold\"},{\"text\":\"]: \",\"color\":\"white\"},{\"text\":\"Show next page\",\"color\":\"aqua\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"{PREFIX}help " + String.valueOf(i) + "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Show help page " + String.valueOf(i) + "\",\"color\":\"gold\"}]}}},{\"text\":\".\",\"color\":\"aqua\"}]}";
    	msg = msg.replace("{PREFIX}", PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).getPrefix());
    	sendRawMessage(p, msg);
    }
    
    void sendCommandHelp(Player p, Command cmd) {
    	String msg = "{\"text\":\"\",\"extra\":[{\"text\":\"[\",\"color\":\"white\"},{\"text\":\"PI3\",\"color\":\"gold\"},{\"text\":\"]: \",\"color\":\"white\"},{\"text\":\"{COMMAND_SYNTAX} - {COMMAND_DESCRIPTION}\",\"color\":\"gold\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"{PREFIX}{COMMAND_NAME}\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"{COMMAND_EXAMPLE}\",\"color\":\"aqua\"}]}}}]}";
    	msg = msg.replace("{COMMAND_NAME}", cmd.getCommandName());
    	msg = msg.replace("{COMMAND_DESCRIPTION}", cmd.getCommandDescription());
    	msg = msg.replace("{COMMAND_SYNTAX}", cmd.getCommandSyntax());
    	msg = msg.replace("{COMMAND_EXAMPLE}", cmd.getCommandExample());
    	msg = msg.replace("{PREFIX}", PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).getPrefix());
    	sendRawMessage(p, msg);
    }
	
	public static void sendRawMessage(Player player, String message) {
		try {
			Object handle = getHandle(player);
			Object connection = getField(handle.getClass(), "playerConnection").get(handle);
			Object serialized = getMethod(nmsChatSerializer, "a", new Class[] { String.class }).invoke(null, new Object[] { message });
			Object packet = nmsPacketPlayOutChat.getConstructor(new Class[] { getNMSClass("IChatBaseComponent") }).newInstance(new Object[] { serialized });
			getMethod(connection.getClass(), "sendPacket", new Class[0]).invoke(connection, new Object[] { packet });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getVersion() {
		String name = Bukkit.getServer().getClass().getPackage().getName();
		String version = name.substring(name.lastIndexOf('.') + 1) + ".";
		return version;
	}
	  
	public static Class<?> getNMSClass(String className) {
		String fullName = "net.minecraft.server." + getVersion() + className;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(fullName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clazz;
	}
	  
	public static Object getHandle(Object obj) {
		try {
			return getMethod(obj.getClass(), "getHandle", new Class[0]).invoke(obj, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	  
	public static Field getField(Class<?> clazz, String name) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	  
	public static Method getMethod(Class<?> clazz, String name, Class<?>... args) {
		for (Method m : clazz.getMethods()) {
			if ((m.getName().equals(name)) && ((args.length == 0) || (ClassListEqual(args, m.getParameterTypes())))) {
				m.setAccessible(true);
				return m;
			}
		}
	    return null;
	}
	  
	public static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2) {
		boolean equal = true;
		if (l1.length != l2.length) {
			return false;
		}
		for (int i = 0; i < l1.length; i++) {
			if (l1[i] != l2[i]) {
				equal = false;
				break;
			}
		}
		return equal;
	}
}
