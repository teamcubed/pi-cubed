package uk.cubed.pi.command.exe;

import org.bukkit.entity.Player;
import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;
import uk.cubed.pi.gui.*;

public class CommandGUI extends Command {

	public CommandGUI() {
		super("GUI");
		this.setCommandDescription("Shows the Inventory GUI.");
		this.setCommandSyntax("{PREFIX}GUI");
		this.setCommandCategory(Category.NULL);
		this.setCommandExample("{PREFIX}GUI");
		this.setMaxArgs(0);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		GUI gui = pi.getGUIManager().getGUI("main");
		gui.showInventory(p);
		PI.sendMessage(p, "Opening GUI...");
	}
}
