package uk.cubed.pi.command.exe;

import java.util.LinkedList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandLoopkill extends Command {
	
	private LinkedList<UUID> loopkillPlayers = new LinkedList<UUID>();
	
	public CommandLoopkill() {
		super("Loopkill");
		this.setCommandDescription("Loopkills one or all players.");
		this.setCommandSyntax("{PREFIX}Loopkill <PlayerName | all>");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}Loopkill Notch");
		this.setMinArgs(1);
		this.setMaxArgs(1);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(PI.getInstance().getPluginInstance(), new Runnable() {
			@Override
			public void run() {
				for (UUID u: loopkillPlayers) {
					Bukkit.getPlayer(u).setHealth(0);
				}
			}
		}, 12000L, 6000L);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		if (args[0].equalsIgnoreCase("all")) {
			if (PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
	    		for (Player v: Bukkit.getOnlinePlayers()) {
	    			if (!pi.getCommandSettings().getVerifiedPlayers().containsKey(v)) {
	    				if (!loopkillPlayers.contains(v.getUniqueId())) {
	    					loopkillPlayers.add(v.getUniqueId());
	    					PI.sendMessage(p, v.getName() + " added to loop kill list");
	    				}
	    			}
	    		}
			} else {
				PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually loopkill.");
			}
		} else {
			Player v = getPlayer(args[0], p);
			if (v == null) {
				PI.sendMessage(p, "Target player could not be found. Command failed.");
			} else if (v == p) {
				PI.sendMessage(p, "Target player is initialised. Command failed.");
			} else {
				if (!loopkillPlayers.contains(v.getUniqueId())) {
					loopkillPlayers.add(v.getUniqueId());
					PI.sendMessage(p, v.getName() + " added to loop kill list.");
				} else {
					loopkillPlayers.remove(v.getUniqueId());
					PI.sendMessage(p, v.getName() + " removed from loop kill list.");
				}
			}
		}
	}

}
