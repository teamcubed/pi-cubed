package uk.cubed.pi.command.exe;

import java.util.LinkedList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandBlockcommand extends Command {
	
	private LinkedList<String> blockedCommands = new LinkedList<String>();

	public CommandBlockcommand() {
		super("Blockcommand");
		this.setCommandDescription("Blocks commands from use by non-initialised players.");
		this.setCommandSyntax("{PREFIX}Blockcommand <Command>");
		this.setCommandCategory(Category.SERVER);
		this.setCommandExample("{PREFIX}Blockcommand /ban");
		this.setMinArgs(1);
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		String cmd = args[0].toLowerCase();
		if (!cmd.startsWith("/")) cmd = "/" + cmd;
		if (blockedCommands.contains(cmd)) {
			blockedCommands.remove(cmd);
			PI.sendMessage(p, args[0] + " is no longer blocked.");
		} else {
			blockedCommands.add(cmd);
			PI.sendMessage(p, args[0] + " is now blocked.");
		}
	}

	public LinkedList<String> getBlockedCommands() {
		return blockedCommands;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void preCommand(PlayerCommandPreprocessEvent e) {
		if (PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) return;
		String cmd = e.getMessage().toLowerCase().split(" ")[0];
		if (blockedCommands.contains(cmd)) e.setCancelled(true);
	}
}
