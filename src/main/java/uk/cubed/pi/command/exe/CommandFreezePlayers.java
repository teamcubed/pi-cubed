package uk.cubed.pi.command.exe;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.bukkit.inventory.ItemStack;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandFreezePlayers extends Command {

	public CommandFreezePlayers() {
		super("FreezePlayers");
		this.setCommandDescription("Toggles the freezing all non-verified players.");
		this.setCommandSyntax("{PREFIX}FreezePlayers");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}FreezePlayers");
		this.setMaxArgs(0);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		setCommandState(!getCommandState());
		if (getCommandState()) {
			PI.sendMessage(p, "Players are now frozen.");
		} else {
			PI.sendMessage(p, "Players are no longer frozen.");
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onBedEnter(PlayerBedEnterEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onBucketEmtpy(PlayerBucketEmptyEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onBucketFill(PlayerBucketFillEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onTabComplete(PlayerChatTabCompleteEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.getTabCompletions().clear();
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onDrop(PlayerDropItemEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onEgg(PlayerEggThrowEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) {
			e.setHatching(false);
			e.getPlayer().getInventory().addItem(new ItemStack(Material.EGG, 1));
			e.getPlayer().updateInventory();
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onExp(PlayerExpChangeEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setAmount(0);
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onFish(PlayerFishEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onGamemode(PlayerGameModeChangeEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onInteractEntity(PlayerInteractEntityEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onInteract(PlayerInteractEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onItemHeld(PlayerItemHeldEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onMove(PlayerMoveEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onItemPickup(PlayerPickupItemEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onPortal(PlayerPortalEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onShearEntity(PlayerShearEntityEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onFlight(PlayerToggleFlightEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onSneak(PlayerToggleSneakEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onSprint(PlayerToggleSprintEvent e) {
		if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) e.setCancelled(getCommandState());
	}
}
