package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;
import uk.cubed.pi.runnables.Kick;

public class CommandKick extends Command {
	
	public CommandKick() {
		super("Kick");
		this.setCommandDescription("Kicks a non-verified player from the server.");
		this.setCommandSyntax("{PREFIX}Kick <PlayerName> [reason]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}Kick Notch You're a faggot.");
		this.setMinArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		String reason = "Disconnected.";
		if (args.length > 1) {
			reason = "";
			for (int i = 1; i < args.length; i++) {
				if (reason.length() == 0) {
					reason = args[i];
				} else {
					reason = reason + " " + args[i];
				}
			}
		}		
		if (args[0].equalsIgnoreCase("all")) {
			if (PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
	    		for (Player v: Bukkit.getOnlinePlayers()) {
	    			if (!pi.getCommandSettings().getVerifiedPlayers().containsKey(v)) {
	    				schedule(new Kick(v, reason));
	    			}
	    		}
			} else {
				PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually kick.");
			}
		} else {
			Player v = getPlayer(args[0], p);
			if (v == null) {
				PI.sendMessage(p, "Target player could not be found. Command failed.");
			} else if (v == p) {
				PI.sendMessage(p, "Target player is initialised. Command failed.");
			} else {
				schedule(new Kick(v, reason));
			}
		}
	}
}
