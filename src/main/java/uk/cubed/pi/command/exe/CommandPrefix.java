package uk.cubed.pi.command.exe;

import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandPrefix extends Command {

	public CommandPrefix() {
		super("Prefix");
		this.setCommandDescription("Changes your command prefix.");
		this.setCommandSyntax("{PREFIX}Prefix <New Prefix>");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}Prefix @");
		this.setMinArgs(1);
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).setPrefix(args[0]);
		PI.sendMessage(p, "Your new prefix is: " + args[0]);
	}

}
