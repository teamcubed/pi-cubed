package uk.cubed.pi.command.exe;

import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;

public class CommandAchievements extends Command {

	public CommandAchievements() {
		super("Achievements");
		this.setCommandDescription("Give all achievements to one or all players.");
		this.setCommandSyntax("{PREFIX}Achievements [player | all]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}Achievements Notch");
		this.setMaxArgs(1);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("all")) {
				if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
					for (Player v: Bukkit.getOnlinePlayers()) {
						for (Achievement a: Achievement.values()) {
							if (!v.hasAchievement(a)) v.awardAchievement(a);
						}
						PI.sendMessage(p, "Awarded " + v.getName() + " all achievements.");
					}
				} else {
					PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually give achievements.");
				}
			} else {
				Player v = getAnyPlayer(args[0]);
				if (v == null) {
					PI.sendMessage(p, "Target player could not be found. Command failed.");
				} else {
					for (Achievement a: Achievement.values()) {
						if (!v.hasAchievement(a)) v.awardAchievement(a);
					}
					PI.sendMessage(p, "Awarded " + v.getName() + " all achievements.");
				}
			}
		} else {
			for (Achievement a: Achievement.values()) {
				if (!p.hasAchievement(a)) p.awardAchievement(a);
			}
			PI.sendMessage(p, "Awarded you all achievements.");
		}
	}
}
