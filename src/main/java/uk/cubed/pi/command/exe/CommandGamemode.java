package uk.cubed.pi.command.exe;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import uk.cubed.pi.command.Command;
import uk.cubed.pi.core.PI;
import uk.cubed.pi.runnables.ChangeGamemode;

public class CommandGamemode extends Command {

	public CommandGamemode() {
		super("GameMode");
		this.setCommandDescription("Changes gamemode for you or others.");
		this.setCommandSyntax("{PREFIX}GameMode <Survival | Creative | Adventure | Spectator | 0 | 1 | 2 | 3> [PlayerName | all]");
		this.setCommandCategory(Category.PLAYER);
		this.setCommandExample("{PREFIX}GameMode Creative");
		this.setMinArgs(1);
		this.setMaxArgs(2);
	}

	@Override
	public void runCommand(Player p, String[] args, PI pi) {
		GameMode gm = null;
		if (pi.getCommandSettings().getGms().containsKey(args[0].toLowerCase())) {
			gm = pi.getCommandSettings().getGms().get(args[0].toLowerCase());
		} else {
			PI.sendMessage(p, "Invalid gamemode.");
			return;
		}
		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("all")) {
				if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDonator() || pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
		    		for (Player v: Bukkit.getOnlinePlayers()) {
		    			schedule(new ChangeGamemode(v, gm));
						PI.sendMessage(p, "Set " + v.getName() + "'s gamemode to " + args[0] + ".");
		    		}
				} else {
					PI.sendMessage(p, "You must be a donator to use the \"all\" parameter. You can still manually change gamemode.");
				}
	    	} else {
				Player v = getPlayer(args[1], p);
				if (v == null) {
					PI.sendMessage(p, "Target player could not be found. Command failed.");
				} else if (v == p) {
					PI.sendMessage(p, "Target player is initialised. Command failed.");
				} else {
					schedule(new ChangeGamemode(v, gm));
					PI.sendMessage(p, "Set " + v.getName() + "'s gamemode to " + args[0] + ".");
				}
	    	}
		} else {
			schedule(new ChangeGamemode(p, gm));
		}
	}

}
