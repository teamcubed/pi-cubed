package uk.cubed.pi.command;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import uk.cubed.pi.core.PI;
import uk.cubed.pi.settings.InitialisedPlayer;

/**
 * @author Sparks
 *
 * Time: 18:03:17
 * Date: 9 Dec 2014
 */
public class CommandManager implements Listener {

	private TreeMap<String, Command> commandMap = new TreeMap<>();
	
	public CommandManager() {
		Bukkit.getPluginManager().registerEvents(this, PI.getInstance().getPluginInstance());
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPiCommand(PlayerCommandPreprocessEvent e) {
		if(e.getMessage().equalsIgnoreCase("/PI")) {
			e.setCancelled(true);
			if (!PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) {
				PI.getInstance().getCommandSettings().getVerifiedPlayers().put(e.getPlayer().getUniqueId(), new InitialisedPlayer(e.getPlayer()));
				//PI.sendMessage(e.getPlayer(), "You are now verified. Messages starting with * will now be hidden.");
				showInitialisedTitle(e.getPlayer(), true);
			} else {
				PI.getInstance().getCommandSettings().getVerifiedPlayers().remove(e.getPlayer().getUniqueId());
				//PI.sendMessage(e.getPlayer(), "You are no longer verified. Messages starting with * will no longer be hidden.");
				showInitialisedTitle(e.getPlayer(), false);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onCommandEvent(AsyncPlayerChatEvent e) {
		UUID p = e.getPlayer().getUniqueId();
		String m = e.getMessage();
		String c = m.split(" ")[0].toLowerCase();
		
		if(PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(p)) {
			if(m.startsWith(PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p).getPrefix())) {
				e.setCancelled(true);
				c = c.substring(PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p).getPrefix().length(), c.length());
				if (commandMap.containsKey(c)) {
					String[] args = Arrays.copyOfRange(m.split(" "), 1, m.split(" ").length);
					int i = args.length;
					Command cmd = commandMap.get(c);
					if (i >= cmd.getMinArgs() && i <= cmd.getMaxArgs()) {
						cmd.runCommand(Bukkit.getPlayer(p), args, PI.getInstance());
					} else {
						PI.sendMessage(Bukkit.getPlayer(p), "Invalid parameters. Usage:");
						PI.sendMessage(Bukkit.getPlayer(p), cmd.getCommandSyntax().replace("{PREFIX}", PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p).getPrefix()));
					}
				} else {
					PI.sendMessage(Bukkit.getPlayer(p), "Unknown command. Run " + PI.getInstance().getCommandSettings().getVerifiedPlayers().get(p).getPrefix() + "Help for command list.");
				}
			}
		}
	}
	
	public TreeMap<String, Command> getCommandMap() {
		return commandMap;
	}
	
	public Command getCommand(String commandName) {
		if(commandMap.containsKey(commandName.toLowerCase())) {
			return commandMap.get(commandName.toLowerCase());
		}
		return null;
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if (PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) 
			PI.getInstance().getCommandSettings().getVerifiedPlayers().remove(e.getPlayer());
	}
	
	@EventHandler
	public void onKick(PlayerKickEvent e) {
		if (PI.getInstance().getCommandSettings().getVerifiedPlayers().containsKey(e.getPlayer().getUniqueId())) 
			PI.getInstance().getCommandSettings().getVerifiedPlayers().remove(e.getPlayer());
	}
	

	public void showInitialisedTitle(Player player, boolean bool) {
		try {
			String title = "{\"text\":\"\",\"extra\":[{\"text\":\"Now Initialised.\",\"color\":\"gold\"}]}";
			if (!bool) title = "{\"text\":\"\",\"extra\":[{\"text\":\"No longer Initialised.\",\"color\":\"gold\"}]}";
			String subtitle = "{\"text\":\"\",\"extra\":[{\"text\":\"" + player.getName() + "\",\"color\":\"aqua\"}]}";
			Object handle = getHandle(player);
			Object connection = getField(handle.getClass(), "playerConnection").get(handle);
			Class<?> enumTitleAction = getNMSClass("EnumTitleAction").getEnumConstants()[0].getClass();
			Object serializedTitle = getMethod(getNMSClass("ChatSerializer"), "a", new Class[] { String.class }).invoke(null, new Object[] { title });
			Object serializedSubtitle = getMethod(getNMSClass("ChatSerializer"), "a", new Class[] { String.class }).invoke(null, new Object[] { subtitle });
			Object packet = getNMSClass("PacketPlayOutTitle").getConstructor(new Class[] { enumTitleAction, getNMSClass("IChatBaseComponent") }).newInstance(new Object[] { getEnumTitleAction("Title"), serializedTitle });
			Object packet2 = getNMSClass("PacketPlayOutTitle").getConstructor(new Class[] { enumTitleAction, getNMSClass("IChatBaseComponent") }).newInstance(new Object[] { getEnumTitleAction("Subtitle"), serializedSubtitle });
			getMethod(connection.getClass(), "sendPacket", new Class[0]).invoke(connection, new Object[] { packet });
			getMethod(connection.getClass(), "sendPacket", new Class[0]).invoke(connection, new Object[] { packet2 });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Object getEnumTitleAction(String s){
		try {
			Class<?> EnumParticleClass = getNMSClass("EnumTitleAction").getEnumConstants()[0].getClass();
			Method a = EnumParticleClass.getDeclaredMethod("a", String.class);
			Object EnumParticle = a.invoke(EnumParticleClass, s);
			return EnumParticle;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	} 	
	
	public static String getVersion() {
		String name = Bukkit.getServer().getClass().getPackage().getName();
		String version = name.substring(name.lastIndexOf('.') + 1) + ".";
		return version;
	}
	  
	public static Class<?> getNMSClass(String className) {
		String fullName = "net.minecraft.server." + getVersion() + className;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(fullName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clazz;
	}
	  
	public static Object getHandle(Object obj) {
		try {
			return getMethod(obj.getClass(), "getHandle", new Class[0]).invoke(obj, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	  
	public static Field getField(Class<?> clazz, String name) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	  
	public static Method getMethod(Class<?> clazz, String name, Class<?>... args) {
		for (Method m : clazz.getMethods()) {
			if ((m.getName().equals(name)) && ((args.length == 0) || (ClassListEqual(args, m.getParameterTypes())))) {
				m.setAccessible(true);
				return m;
			}
		}
	    return null;
	}
	  
	public static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2) {
		boolean equal = true;
		if (l1.length != l2.length) {
			return false;
		}
		for (int i = 0; i < l1.length; i++) {
			if (l1[i] != l2[i]) {
				equal = false;
				break;
			}
		}
		return equal;
	}
}
