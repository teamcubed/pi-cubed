package uk.cubed.pi.command;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.bukkit.Bukkit;

import uk.cubed.pi.core.PI;

/**
 * @author Sparks
 *
 * Time: 18:03:10
 * Date: 9 Dec 2014
 */
public class CommandLoader {
	
	private String pkg;
	
	public CommandLoader(String pkg) {
		this.pkg = pkg;
		loadCommands();
	}
	
	public void loadCommands() {
        try {
    		String jarPath = "";
			try {
				jarPath = PI.class.getProtectionDomain().getCodeSource().getLocation().toURI().toString().substring(5).replace("%20", " ");
			} catch (URISyntaxException e1) {
				e1.printStackTrace();
			}
        	
            @SuppressWarnings("resource")
			JarFile jarFile = new JarFile(jarPath);
            Enumeration<JarEntry> entries = jarFile.entries();
            JarEntry jarEntry;
            while(entries.hasMoreElements()) {
                jarEntry = entries.nextElement();
                if(jarEntry.getName().startsWith(pkg) && jarEntry.getName().endsWith(".class")) {
                    String className = jarEntry.getName().replaceAll("/", "\\.");
                    className = className.substring(0, className.length() - 6);
                    if(className.contains("$")) continue;
                    ClassLoader classLoader = this.getClass().getClassLoader();
                    URL url = new URL("jar:file:" + jarPath + "!/");

                    @SuppressWarnings("resource")
					URLClassLoader uclLoader = new URLClassLoader(new URL[]{url}, classLoader);

                    try {
                        Class<?> commands = uclLoader.loadClass(className);
                        Command command = (Command) commands.newInstance();
                        Bukkit.getPluginManager().registerEvents(command, PI.getInstance().getPluginInstance());
                        PI.getInstance().getCommandManager().getCommandMap().put(command.getCommandName().toLowerCase(), command);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}
