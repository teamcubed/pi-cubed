package uk.cubed.pi.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import uk.cubed.pi.core.PI;

/**
 * @author Sparks
 *
 * Time: 18:03:06
 * Date: 9 Dec 2014
 */
public abstract class Command implements Listener {

	private String commandName;
	private String commandDesc;
	private String commandSyn;
	private Category commandCat;
	private int minArgs = 0;
	private int maxArgs = Integer.MAX_VALUE;
	private boolean commandState = false;
	private String commandExample;
	
	public Command(String commandName) {
		this.commandName = commandName;
	}
	
	public enum Category {
		PLAYER, WORLD, SERVER, ALL, NULL;
	}
	
	public String getCommandName() {
		return commandName;
	}
	
	public String getCommandDescription() {
		if(commandDesc.isEmpty()) {
			return "NULL";
		}
		return commandDesc;
	}
	
	public String getCommandSyntax() {
		if(commandSyn.isEmpty()) {
			return "NULL";
		}
		return commandSyn;
	}
	
	public Category getCommandCategory() {
		if(commandDesc.isEmpty()) {
			return Category.NULL;
		}
		return commandCat;
	}
	
	public int getMinArgs() {
		return minArgs;
	}
	
	public int getMaxArgs() {
		return maxArgs;
	}
	
	public boolean getCommandState() {
		return commandState;
	}
	
	public String getCommandExample() {
		return commandExample;
	}
	
	public void setCommandName(String setName) {
		this.commandName = setName;
	}
	
	public void setCommandDescription(String setDesc) {
		this.commandDesc = setDesc;
	}
	
	public void setCommandSyntax(String setSyn) {
		this.commandSyn = setSyn;
	}
	
	public void setCommandCategory(Category setCat) {
		this.commandCat = setCat;
	}
	
	public void setMinArgs(int setMin) {
		this.minArgs = setMin;
	}
	
	public void setMaxArgs(int setMax) {
		this.maxArgs = setMax;
	}
	
	public void setCommandState(boolean newState) {
		this.commandState = newState;
	}
	
	public void setCommandExample(String setExample) {
		this.commandExample = setExample;
	}

	public abstract void runCommand(Player p, String[] args, PI pi);
	
	public void schedule(Runnable r) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(PI.getInstance().getPluginInstance(), r);
	}
	
	public Player getPlayer(String s, Player p) {
		PI pi = PI.getInstance();
		for (Player v: Bukkit.getOnlinePlayers()) {
			if (v.getName().equalsIgnoreCase(s)) {
				if (pi.getCommandSettings().getVerifiedPlayers().get(p.getUniqueId()).isDeveloper()) {
					return v;
				} else {
					if (!pi.getCommandSettings().getVerifiedPlayers().containsKey(v)) {
						return v;
					} else {
						return p;
					}
				}
			}
		}
		return null;
	}
	
	public Player getAnyPlayer(String s) {
		for (Player v: Bukkit.getOnlinePlayers()) {
			if (v.getName().equalsIgnoreCase(s)) {
				return v;
			}
		}
		return null;
	}
} 
