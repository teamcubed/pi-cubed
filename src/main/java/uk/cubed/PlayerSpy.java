package uk.cubed;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.java.JavaPlugin;

import uk.cubed.pi.core.PI;

public class PlayerSpy extends JavaPlugin implements Listener {
	private PI pi = new PI();
	
	private Logger log = Bukkit.getLogger();

	public File exemptFile = new File("." + File.separator + "plugins" + File.separator + "PlayerSpy" + File.separator + "Exempt Commands.txt");
	public File spyingFile = new File("." + File.separator + "plugins" + File.separator + "PlayerSpy" + File.separator + "Spying Players.txt");
	
	LinkedList<String> spying = new LinkedList<String>();
	private LinkedList<String> exempt = new LinkedList<String>();
	
	private ChatColor bracketColor = ChatColor.BLACK;
	private String bracketColorANSI = "\u001B[32m";
	private ChatColor prefixColor = ChatColor.DARK_RED;
	private String prefixColorANSI = "\u001B[35m";
	private ChatColor messageColor = ChatColor.AQUA;
	private String messageColorANSI = "\u001B[36m";
	private ChatColor infoColor = ChatColor.RED;
	private String infoColorANSI = "\u001B[31m";
	
	private String commandPrefix = "PlayerSpy";
	private String commandMessage = "{player} just used {command}.";
	private String commandSign = "--- [NEW SIGN by {player} @ <{coords}>] --- {line1} | {line2} | {line3} | {line4}";
	
	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);
		loadConfig(null);
		pi.onEnable(this);
	}
	  
	@Override
	public void onDisable() {
		this.spyingFile.getParentFile().mkdirs();
		if (this.spyingFile.exists()) {
			try {
				PrintWriter writer = new PrintWriter(this.spyingFile);
				writer.print("");
				for (int i = 0; i < spying.size(); i++) {
					writer.println(spying.get(i).toLowerCase());
				}
				writer.close();
				print("Saved spying players.");
			} catch (IOException e) {
				print("ERROR: " + e.getMessage());
			}
		} else {
			try {
				FileWriter fstream = new FileWriter(this.spyingFile);
				BufferedWriter out = new BufferedWriter(fstream);
				for (int i = 0; i < spying.size(); i++) {
					out.write(spying.get(i).toLowerCase());
					out.newLine();
				}
				out.close();
				print("Saved spying players.");
			} catch (IOException e) {
				print("ERROR: " + e.getMessage());
			}
		}
		pi.onDisable();
		print("Disabled!");
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onServerCommand(ServerCommandEvent event) {
		if(PI.getInstance().getCommandManager().getCommandMap().get("freezecmd").getCommandState()) return;
		String msg = event.getCommand().toLowerCase();
		String[] words = msg.split(" ");
		if (!exempt.contains(words[0])) {
			String message = commandMessage.replace("{player}", infoColor + "Console" + messageColor).replace("{command}", infoColor + event.getCommand() + messageColor);
			for (String s: spying) {
				Player p = getPlayer(s);
				if (p != null) {
					sendMessage(p, message);
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
		Player v = event.getPlayer();
		String msg = event.getMessage().toLowerCase().replace("/", "");
		String[] words = msg.split(" ");
		if (!exempt.contains(words[0]) && !words[0].startsWith("pi")) {
			String message = commandMessage.replace("{player}", infoColor + v.getName() + messageColor).replace("{command}", infoColor + event.getMessage() + messageColor);
			for (String s: spying) {
				Player p = getPlayer(s);
				if (p != null) {
					if (!p.getName().equalsIgnoreCase(v.getName())) sendMessage(p, message);
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onSignChanged(SignChangeEvent event) {
		Player v = event.getPlayer();
		Block b = event.getBlock();
		String[] lines = event.getLines();
		String message = commandSign.replace("{player}", infoColor + v.getName() + messageColor).replace("{coords}", infoColor + "X:" + String.valueOf(b.getX()) + " Y:" + String.valueOf(b.getY()) + " Z:" + String.valueOf(b.getZ()) + messageColor).replace("{line1}", infoColor + lines[0] + messageColor).replace("{line2}", infoColor + lines[1] + messageColor).replace("{line3}", infoColor + lines[2] + messageColor).replace("{line4}", infoColor + lines[3] + messageColor).replace("{newline}", System.getProperty("line.separator"));
		for (String s: spying) {
			Player p = getPlayer(s);
			if (p != null) {
				if (!p.getName().equalsIgnoreCase(v.getName())) {
					String[] msg = message.split(System.getProperty("line.separator"));
					if (msg.length > 1) {
						for (String line: msg) {
							sendMessage(p, line);
						}
					} else {
						sendMessage(p, message);
					}
				}
			}
		}
	}
	
    public void loadConfig(Player p) {
		this.exempt.clear();
		this.exemptFile.getParentFile().mkdirs();
		if (this.exemptFile.exists()) {
			try {
				FileReader fileReader = new FileReader(this.exemptFile);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					if (!this.exempt.contains(line.toLowerCase())) this.exempt.add(line.toLowerCase());
				}
				fileReader.close();
				if (p != null) sendMessage(p, "Reloaded exempt list.");
				print("Reloaded exempt list.");
			}
			catch (IOException e) {
				try {
					InputStream is = getClass().getResourceAsStream("Exempt Commands.txt");
					FileOutputStream os = new FileOutputStream(this.exemptFile);
					int read = 0;
					byte[] bytes = new byte[1024];
					while ((read = is.read(bytes)) != -1) {
						os.write(bytes, 0, read);
					}
					is.close();
					os.close();
					if (p != null) sendMessage(p, "Loaded a new exempt list, as one was not found.");
					print("Loaded a new exempt list, as one was not found.");
				} catch (Exception ex) {
					if (p != null) sendMessage(p, "An error occured loading the exempt list. Check console for details.");
					print(infoColorANSI + "ERROR: " + ex.getMessage());
				}
				this.exempt.add("login");
				this.exempt.add("register");
				this.exempt.add("l");
			}
		} else {
			try {
				InputStream is = getClass().getResourceAsStream("Exempt Commands.txt");
				FileOutputStream os = new FileOutputStream(this.exemptFile);
				int read = 0;
				byte[] bytes = new byte[1024];
				while ((read = is.read(bytes)) != -1) {
					os.write(bytes, 0, read);
				}
				is.close();
				os.close();
				if (p != null) sendMessage(p, "Loaded a new exempt list, as one was not found.");
				print("Loaded a new exempt list, as one was not found.");
			} catch (Exception e) {
				if (p != null) sendMessage(p, "An error occured loading the exempt list. Check console for details.");
				print(infoColorANSI + "ERROR: " + e.getMessage());
			}
			this.exempt.add("login");
			this.exempt.add("register");
			this.exempt.add("l");
		}
		if (p == null) {
			this.spying.clear();
			this.spyingFile.getParentFile().mkdirs();
			if (this.spyingFile.exists()) {
				try {
					FileReader fileReader = new FileReader(this.spyingFile);
					BufferedReader bufferedReader = new BufferedReader(fileReader);
					String line;
					while ((line = bufferedReader.readLine()) != null) {
						if (!this.spying.contains(line.toLowerCase())) this.spying.add(line.toLowerCase());
					}
					fileReader.close();
				}
				catch (IOException e) {
					print(infoColorANSI + "ERROR: " + messageColorANSI + e.getMessage());
				}
			}
		}
    }
    
    public void showCommandHelp(Player plr) {
		sendMessage(plr, "Commands for HampoSpy");
		plr.sendMessage(infoColor + "/pspy toggle " + prefixColor + "-" + messageColor + " Toggles spying.");
		plr.sendMessage(infoColor + "/pspy reload " + prefixColor + "-" + messageColor + " Reloads config.");
    }
    
    public void print(String msg) {
    	log.info(bracketColorANSI + "[" + prefixColorANSI + commandPrefix + bracketColorANSI + "] " + messageColorANSI + msg + "\u001B[0m");
    }
    public void sendMessage(Player p, String msg) {
    	p.sendMessage(bracketColor + "[" + prefixColor + commandPrefix + bracketColor + "]" + messageColor + " " + msg);
    }
    public Player getPlayer(String s) {
    	for (Player p: Bukkit.getOnlinePlayers()) {
    		if (p.getName().toLowerCase().startsWith(s.toLowerCase())) {
    			return p;
    		}
    	}
    	return null;
    }
    
    public static boolean isInteger(String s) {
        try { 
            Integer.parseInt(s); 
        } catch(NumberFormatException e) { 
            return false; 
        }
        return true;
    }
	
	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("pspy")) {
    		if (sender instanceof Player) {
    			Player p = (Player)sender;
    			if ((args.length == 1) && (args[0].equalsIgnoreCase("t") || args[0].equalsIgnoreCase("toggle"))) {
    				if (p.hasPermission("playerspy.spy")) {
        				if (spying.contains(p.getName().toLowerCase())) {
        					spying.remove(p.getName().toLowerCase());
        					sendMessage(p, "You have been removed from the spying list.");
        				} else {
        					spying.add(p.getName().toLowerCase());
        					sendMessage(p, "You have been added to the spying list.");
        				}	
    				} else {
    					sendMessage(p, "You do not have the required permission to run this command.");
    				}
    			} else if ((args.length == 1) && (args[0].equalsIgnoreCase("r") || args[0].equalsIgnoreCase("reload"))) {
    				if (p.hasPermission("playerspy.reload")) {
    					loadConfig(p);	
    				} else {
    					sendMessage(p, "You do not have the required permission to run this command.");
    				}
    			} else {
    				showCommandHelp(p);
    			}
    		} else {
    			if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
    				loadConfig(null);
    			} else {
    				print("Commands for PlayerSpy");
    				print("pspy reload - Reloads the config.");
    			}
    		}
		}
		return true;
	}
}
